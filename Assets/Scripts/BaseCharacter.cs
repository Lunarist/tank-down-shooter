using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter : MonoBehaviour
{
    [SerializeField] string charName;

    [SerializeField] int damage;
    protected int currHP;
    protected int maxHP;

    [SerializeField] protected float fireRate;
    protected float nextFire;

    protected Rigidbody rb;

    protected BulletPooling bulletPooling;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        currHP = maxHP;
        rb = GetComponent<Rigidbody>();
        bulletPooling = FindObjectOfType<BulletPooling>();
    }

    protected virtual void Movement()
    {
        // Movement
    }

    protected virtual void FiringMove()
    {
        // Firing
    }

    public void GotHurt(int dmg)
    {
        currHP -= dmg;
    }

    protected virtual void Death()
    {
        
    }
}
