using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float rotateSpeed = 20f;

    Camera mainCamera;

    public GameObject head;
    
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float rotateInput = Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;
        float forwardInput = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(0, 0, forwardInput);
        transform.Rotate(0, rotateInput, 0);

        PointingGun();
    }

    void PointingGun()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (ground.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);

            // transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            head.transform.LookAt(pointToLook);
            // head.transform.LookAt(new Vector3(0, transform.rotation.eulerAngles.y, 0));
            head.transform.rotation = Quaternion.Euler(new Vector3(0, head.transform.rotation.eulerAngles.y, 0));
        }
    }
}
