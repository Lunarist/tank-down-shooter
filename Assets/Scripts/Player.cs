using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseCharacter
{
    public GameObject head;
    
    Camera mainCamera;

    public Transform firePoint;
    // Vector3 moveInput;
    // Vector3 moveVelocity;

    [SerializeField] float speed = 10f;
    [SerializeField] float rotateSpeed = 20f;

    bool isFiring;

    protected override void Start()
    {
        base.Start();

        mainCamera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        PointingGun();

        nextFire -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            isFiring = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isFiring = false;
        }

        if (isFiring)
        {
            if (nextFire <= 0)
            {
                nextFire = fireRate;
                FiringMove();
            }
        }
    }

    void FixedUpdate()
    {
        // Movement as a capsule object
        // rb.velocity = moveVelocity;

        Movement();
    }

    protected override void Movement()
    {
        base.Movement();

        // Movement as a capsule object
        // moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        // moveVelocity = moveInput * speed;

        // Movement as a tank
        float rotateInput = Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;
        float forwardInput = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(0, 0, forwardInput);
        transform.Rotate(0, rotateInput, 0);
    }

    // Pointing gun as a capsule object
    // void PointingGun()
    // {
    //     Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
    //     Plane ground = new Plane(Vector3.up, Vector3.zero);
    //     float rayLength;

    //     if (ground.Raycast(cameraRay, out rayLength))
    //     {
    //         Vector3 pointToLook = cameraRay.GetPoint(rayLength);

    //         transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
    //     }
    // }

    void PointingGun()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane ground = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (ground.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

            head.transform.LookAt(pointToLook);
            head.transform.rotation = Quaternion.Euler(new Vector3(0, head.transform.rotation.eulerAngles.y, 0));
        }
    }

    protected override void FiringMove()
    {
        base.FiringMove();

        bulletPooling.Request(firePoint);
        bulletPooling.tag = "Player";
    }
}
