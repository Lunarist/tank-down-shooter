using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Character : ScriptableObject
{
    public string charName;

    public int healthPoint;
    public int damage;

    public float fireRate;

}
