using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{   
    Animator animator;
    BulletPooling bulletPooling;

    public Transform firePoint;

    public GameObject player;
    public GameObject GetPlayer()
    {
        return player;
    }


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        bulletPooling = FindObjectOfType<BulletPooling>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    void Fire()
    {
        bulletPooling.Request(firePoint);
        bulletPooling.tag = "Player";
    }
}
