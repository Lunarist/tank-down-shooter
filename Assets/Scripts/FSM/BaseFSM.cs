using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFSM : StateMachineBehaviour
{
    public GameObject enemy;
    public GameObject player;
    public float speed = 5.0f;
    public float rotateSpeed = 3.0f;
    public float distance = 1.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       enemy = animator.gameObject;
       player = enemy.GetComponent<EnemyAI>().GetPlayer();
    }
}
